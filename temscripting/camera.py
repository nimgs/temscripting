########## (PLATE) CAMERA PROPERTIES ###########
import enums

class Camera(object):

    def __init__(self, com):
        self.com = com

    def get_camera_main_screen():
        return self.com.Camera.MainScreen

    def set_camera_main_screen(value=None):
        position = self.com.Camera.MainScreen
        try:
            if enums.ScreenPosition(position):
                self.com.Camera.MainScreen = value
        except comtypes.COMError:
            raise ValueError

    def get_is_small_screen_down():
        return self.com.Camera.IsSmallScreenDown

    def get_stock():
        return float(self.com.Camera.Stock)

    def get_measured_exposure_time():
        return float(self.com.Camera.MeasuredExposureTime)

    def get_manual_exposure_time():
        return float(self.com.Camera.ManualExposureTime)

    def set_manual_exposure_time(sec=None):
        try:
            self.com.Camera.ManualExposureTime = float(sec)
        except comtypes.COMError:
            raise ValueError

    def get_manual_exposure():
        return self.com.Camera.ManualExposure

    def set_manual_exposure(value=None):
        try:
            self.com.Camera.ManualExposure = value
        except comtypes.COMError:
            raise ValueError

    def get_film_text():
        return self.com.Camera.FilmText

    def set_film_text(text=None):
        try:
            self.com.Camera.FilmText = text
        except comtypes.COMError:
            raise ValueError

    def get_exposure_number():
        return float(self.com.Camera.ExposureNumber)

    def set_exposure_number(value=None):
        try:
            self.comCamera.ExposureNumber = float(value)
        except comtypes.COMError:
            raise ValueError

    def get_user_code():
        return self.com.Camera.UserCode

    def set_user_code(code=None):
        try:
            self.com.Camera.UserCode = code
        except comtypes.COMError:
            raise ValueError

    def get_plate_label_date_type():
        return enums.PlateLabelDateFormat(self.com.Camera.PlateLabelDateType)

    def set_plate_label_date_type(value=None):
        try:
            if enums.PlateLabelDateFormat(value):
                self.com.Camera.PlateLabelDateType = value
        except comtypes.COMError:
            raise ValueError

    def get_screen_dim_text():
        return self.com.Camera.ScreenDimText

    def set_screen_dim_text(value=None):
        try:
            self.com.Camera.ScreenDimText = value
        except comtypes.COMError:
            raise ValueError

    def get_screen_dim():
        return self.com.Camera.ScreenDim

    def set_screen_dim(value=None):
        try:
            self.com.Camera.ScreenDim = value
        except comtypes.COMError:
            raise ValueError

    def get_screen_current():
        return float(self.com.Camera.ScreenCurrent)

    def set_screen_current(amperes=None):
        try:
            self.com.Camera.ScreenCurrent = float(amperes)
        except comtypes.COMError:
            raise ValueError

    def take_exposure():
        try:
            self.com.Camera.TakeExposure
        except comtypes.COMError:
            raise ValueError

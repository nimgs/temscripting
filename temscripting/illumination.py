########## ILLUMINATION PROPERTIES ###########
import enums

class Illumination(object):

    def __init__(self, com):
        self.com = com

    def get_mode():
        return enums.IlluminationMode(self.com.Illumination.Mode)

    def set_mode(value=None):
        if enums.IlluminationMode(value):
            self.com.Illumination.Mode = value

    def get_df_mode():
        return enums.DarkFieldMode(self.com.Illumination.DFMode)

    def set_df_mode(value=None):
        if enums.DarkFieldMode(value):
            self.com.Illumination.DFMode = value

    def get_beam_blanker():
        return self.com.Illumination.BeamBlanked

    def set_beam_blanker(value=None):
        try:
            self.com.Illumination.BeamBlanked = value
        except comtypes.COMError:
            raise ValueError

    def get_condenser_stigmator():
        vec = self.com.Illumination.CondensorStigmator
        return [vec.X, vec.Y]

    def set_condenser_stigmator(x=None, y=None):
        # range: -1.0 to +1.0
        stigmator = [float(x), float(y)]
        stigmator = map(sum, zip(stigmator, self.get_condenser_stigmator()))
        self.com.Illumination.CondenserStigmator = stigmator

    def get_spot_size_index():
        return self.com.Illumination.SpotsizeIndex

    def set_spot_size_index(index=0):
        self.com.Illumination.SpotsizeIndex = index

    def get_intensity():
        return self.com.Illumination.Intensity

    def set_intensity(value=None):
        # some microscopes minimum is higher than
        self.com.Illumination.Intensity = value

    def get_intensity_zoom_enabled():
        return self.com.Illumination.IntensityZoomEnabled

    def set_intensity_zoom_enabled(enable=None):
        self.com.Illumination.IntensityZoomEnabled = enabled

    def get_intensity_limit_enabled():
        return self.com.Illumination.IntensityLimitEnabled

    def set_intensity_limit_enabled(enable=None):
        self.com.Illumination.IntensityLimitEnabled = enable

    def get_illumination_shift():
        vec = self.com.Illumination.Shift.value
        return [vec.X, vec.Y]

    def set_illumination_shift(x=None, y=None):
        shift = [float(x), float(y)]
        shift = map(sum, zip(shift, self.get_illumination_shift()))
        self.com.Illumination.Shift = shift

    def get_illumination_tilt():
        vec = self.com.Illumination.Tilt.value
        return [vec.X, vec.Y]

    def set_illumination_tilt(x=None, y=None):
        tilt = [float(x), float(y)]
        tilt = map(sum, zip(tilt, self.get_illumination_tilt()))
        self.com.Illumination.Tilt = tilt

    def get_illumination_rotation_center():
        vec = self.com.Illumination.RotationCenter
        return [vec.X, vec.Y]

    def set_illumination_rotation_center(x=None, y=None):
        rotation = [float(x), float(y)]
        rotation = map(sum, zip(rotation, self.get_illumination_rotation_center()))
        self.com.Illumination.RotationCenter = rotation

    def get_illumination_stem_magnification():
        return float(self.com.Illumination.StemMagnification)

    def set_illumination_stem_magnification(value=None):
        magnification = self.com.Illumination.StemMagnification
        magnification = float(value)
        try:
            self.com.Illumination.StemMagnification = magnification
        except comtypes.COMError:
            raise ValueError

    def get_illumination_stem_rotation():
        return float(self.com.Illumination.StemRotation)

    def set_illumination_stem_rotation(rotation=None):
        rotation = float(rotation)
        rotation = map(sum, zip(rotation, self.get_illumination_stem_rotation()))
        self.com.Illumination.StemRotation = rotation


import azmq
import zgmp

class Driver(object):

    def __init__(self):
        
        import stage
        import illumination
        import gun
        import projection
        import instrument_mode_control
        import autonormalizedenabled
        import acquisition
        import ccdcameras

        import comtypes.client as cc

        self.com = cc.CreateObject('TEMScripting.Instrument')
        self.stage = stage.Stage(self.com)
        self.illumination = illumination.Illumination(self.com)
        self.gun = gun.Gun(self.com)
        self.projection = projection.Projection(self.com)
        self.instrument_mode_control = instrument_mode_control.InstrumentModeControl(self.com)
        self.autonormalizedenabled = autonormalizedenabled.AutoNormalizedEnabled(self.com)
        self.acquisition = acquisition.Acquisition(self.com)
        self.ccdcameras = ccdcameras.CCDCameras(self.com)

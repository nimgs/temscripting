########## PROJECTION PROPERTIES ###########
import enums

class Projection(object):

    def __init__(self, com):
        self.com = com

    def get_projection_mode():
        return enums.ProjectionMode(self.com.Projection.Mode)

    def set_projection_mode(mode=None):
        if enums.ProjectionMode(mode):
            self.com.Projection.Mode = mode

    def get_projection_submode():
        return enums.ProjectionSubMode(self.com.Projection.SubMode)

    def get_projection_submode_string():
        return self.com.Projection.SubModeString

    def get_projection_lens_program():
        return enums.LensProg(self.Projection.LensProgram)

    def set_projection_lens_program(program=None):
        if enums.LensProg(program):
            self.com.Projection.LensProgram = program

    def get_projection_magnification():
        return float(self.com.Projection.Magnification)

    def get_projection_magnification_index():
        return self.com.Projection.MagnificationIndex

    def set_projection_magnification_index(value=None):
        index = self.com.Projection.MagnificationIndex
        index = value
        try:
            self.com.Projection.MagnificationIndex = index
        except comtypes.COMError:
            raise ValueError

    def get_projection_image_rotation():
        return float(tem.Projection.ImageRotation)

    def get_projection_detector_shift():
        return enums.ProjectionDetectorShift(self.com.Projection.DetectorShift)

    def set_projection_detector_shift(shift=None):
        if enums.ProjectionDetectorShift(shift):
            self.com.Projection.DetectorShift = shift

    def get_projection_detector_shift_mode():
        return enums.ProjDetectorShiftMode(self.com.Projection.DetectorShiftMode)

    def set_projection_detector_shift_mode(mode=None):
        if enums.ProjDetectorShiftMode(mode):
            self.com.Projection.DetectorShiftMode = mode

    def get_projection_focus():
        return float(self.com.Projection.Focus)

    def set_projection_focus(value=None):
        focus = self.com.Projection.Focus
        focus = float(value)
        try:
            self.com.Projection.Focus = focus
        except comtypes.COMError:
            raise ValueError

    def get_projection_defocus():
        return float(self.com.Projection.Defocus)

    def set_projection_defocus(value=None):
        defocus = self.com.Projection.Defocus
        defocus = float(value)
        print 'dval: {}'.format(defocus)
        try:
            self.com.Projection.Defocus = defocus
        except comtypes.COMError:
            raise ValueError

    def get_projection_objective_excitation():
        return float(self.com.Projection.ObjectiveExcitation)

    def get_projection_camera_length():
        return float(self.com.Projection.CameraLength)

    def get_projection_camera_length_index():
        return self.com.Projection.CameraLengthIndex

    def set_projection_camera_length_index(index=None):
        self.com.Projection.CameraLengthIndex = index

    def get_projection_objective_stigmator():
        vec = self.com.Projection.ObjectiveStigmator
        return [vec.X, vec.Y]

    def set_projection_objective_stigmator(x=None, y=None):
        stigmator = [float(x), float(y)]
        stigmator = map(sum, zip(stigmator, self.get_projection_objective_stigmator()))
        self.com.Projection.ObjectiveStigmator = stigmator

    def get_projection_diffraction_stigmator():
        vec = self.com.Projection.DiffractionStigmator
        return [vec.X, vec.Y]

    def set_projection_diffraction_stigmator(x=None, y=None):
        stigmator = [float(x), float(y)]
        stigmator = map(sum, zip(stigmator, self.get_projection_diffraction_stigmator()))
        self.com.Projection.DiffractionStigmator = stigmator

    def get_projection_diffraction_shift():
        vec = self.com.Projection.DiffractionShift
        return [vec.X, vec.Y]

    def set_projection_diffraction_shift(x=None, y=None):
        shift = [float(x), float(y)]
        shift = map(sum, zip(shift, self.get_projection_diffraction_shift()))
        self.com.Projection.DiffractionShift = shift

    def get_projection_image_shift():
        vec = self.com.Projection.ImageShift
        print 'vecx: {} vecy: {}'.format(vec.X, vec.Y)
        return [vec.X, vec.Y]

    def set_projection_image_shift(x=None, y=None):
        shift = [float(x), float(y)]
        shift = map(sum, zip(shift, self.get_projection_image_shift()))
        print 'shift[0]: {}, shift[1]: {}'.format(shift[0], shift[1])
        pis = self.com.Projection.ImageShift
        try:
            pis.X = shift[0]
            pis.Y = shift[1]
            self.com.Projection.ImageShift = pis
        except comtypes.COMError:
            raise ValueError

    def get_projection_image_beam_shift():
        vec = self.com.Projection.ImageBeamShift
        print type(vec)
        print 'vecx: {} vecy: {}'.format(vec.X, vec.Y)
        return [vec.X, vec.Y]

    def set_projection_image_beam_shift(x=None, y=None):
        shift = [float(x), float(y)]
        shift = map(sum, zip(shift, self.get_projection_image_beam_shift()))
        print 'shift[0]: {}, shift[1]: {}'.format(shift[0], shift[1])
        pibs = self.com.Projection.ImageBeamShift
        try:
            pibs.X = shift[0]
            pibs.Y = shift[1]
            self.com.Projection.ImageBeamShift = pibs
            #print 'X = {}'.format(self.com.Projection.ImageBeamShift.X)
        except comtypes.COMError:
            raise ValueError

    def get_projection_image_beam_tilt():
        vec = self.com.Projection.ImageBeamTilt
        return [vec.X, vec.Y]

    def set_projection_image_beam_tilt(x=None, y=None):
        tilt = [float(x), float(y)]
        tilt= map(sum, zip(shift, self.get_projection_image_beam_tilt()))
        self.com.Projection.ImageBeamTilt = tilt

    def get_projection_index():
        return self.com.Projection.ProjectionIndex

    def set_projection_index(index=None):
        self.com.Projection.ProjectionIndex = index

    def get_projection_submode_minindex():
        return self.com.Projection.SubModeMinIndex

    def get_projection_submode_maxindex():
        return self.com.Projection.SubModeMaxIndex

    def set_projection_reset_defocus():
        self.com.Projection.ResetDefocus()
        #set_projection_defocus(value=value)

    def set_change_projection_index(steps=None):
        self.com.Projection.ChangeProjectionIndex = steps

    def set_projection_normalize(value=None):
        try:
            if enums.ProjectionNormalization(value):
                self.com.Projection.Normalize(norm=value)
        except comtypes.COMError:
            return ValueError

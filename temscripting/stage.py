

########## STAGE PROPERTIES ###########

import enums

class Stage(object):

    def __init__(self, com):
        self.com = com

    def get_stage_status(self):
        return enums.StageStatus(self.com.Stage.Status)

    def get_stage_position(self):
        '''Return utility StagePosition object to get/set the current
          position of the stage and return position as an array
          of 5 doubles
        '''
        pos = self.com.Stage.Position
        return {
            'x': pos.X,
            'y': pos.Y,
            'z': pos.Z,
            'a': pos.A,
            'b': pos.B,
        }

    def get_stage_holder(self):
        return enums.StageHolderType(self.com.Stage.Holder)

    def set_stage_position(x=None, y=None, z=None, a=None, b=None):
        self.go_to(x=x, y=y, z=z, a=a, b=b)

    def get_stage_axis_data():
        return self.com.Stage.AxisData

    def norm(self, x=None, y=None, z=None, a=None, b=None):
        '''
        returns a COM stage position object and axes enum for use by
        the go_to and move_to functions
        '''

        axes = 0
        dst = self.com.Stage.Position

        if x is not None: dst.X = x
        if y is not None: dst.Y = y
        if z is not None: dst.Z = z
        if a is not None: dst.A = a
        if b is not None: dst.B = b

        if x is not None:
            axes |= enums.StageAxes.X
        if y is not None:
            axes |= enums.StageAxes.Y
        if z is not None:
            axes |= enums.StageAxes.Z
        if a is not None:
            axes |= enums.StageAxes.A
        if b is not None:
            axes |= enums.StageAxes.B

        return dst, axes


    def go_to(self, x=None, y=None, z=None, a=None, b=None):

        dst, axes = self.norm(x=x, y=y, z=z, a=a, b=b)

        try:
            self.com.Stage.Goto(dst, axes)
        except comtypes.COMError:
            status = self.get_stage_status()
            if status == enums.StageStatus.NotReady:
                raise StageNotReady('error %s: stage is not ready'%status)
            elif status == enums.StageStatus.Disabled:
                raise StageNotReady('error %s: stage is disabled'%status)
            elif status == enums.StageStatus.Wobbling:
                raise StageNotReady('error %s: stage is wobbling'%status )

    def move_to(self, **kwargs):
        dst, axes = self.norm(**kwargs)
        try:
            self.com.Stage.MoveTo(dst, axes)
        except comtypes.COMError:
            status = self.get_stage_status()
            if status == enums.StageStatus.NotReady:
                raise StageNotReady('error %s: stage is not ready'%status)
            elif status == enums.StageStatus.Disabled:
                raise StageNotReady('error %s: stage is disabled'%status)
            elif status == enums.StageStatus.Wobbling:
                raise StageNotReady('error %s: stage is wobbling'%status )

class StageNotReady(Exception):
    pass


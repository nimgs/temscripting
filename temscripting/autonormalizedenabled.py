########## AUTONORMALIZEENABLED NORMALIZEALL PROPERTY ###########
import enums

class AutoNormalizedEnabled(object):

    def __init__(self, com):
        self.com = com

    def get_autonormalize_enabled():
        return self.com.AutoNormalizeEnabled

    def set_autonormalize_enabled(value=None):
        try:
            self.com.AutoNormalizeEnabled = value
        except comtypes.COMError:
            raise ValueError

    def set_normalize_all():
        try:
            self.com.NormalizeAll()
        except comtypes.COMError:
            raise ValueError

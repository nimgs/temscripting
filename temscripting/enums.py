from enum import Enum

# values for enumeration 'ProjectionNormalization'
class ProjectionNormalization(Enum):
    Objective = 10
    Projection = 11
    All = 12


# values for enumeration 'StageAxes'
class StageAxes(Enum):
    X  = 1
    Y  = 2
    XY = 3
    Z  = 4
    A  = 8
    B  = 16

# values for enumeration 'RefrigerantLevel'
class RefrigerantLevel(Enum):
    AutoLoader = 0
    Column     = 1
    Helium     = 2

# values for enumeration 'ProjectionMode'
class ProjectionMode(Enum):
    Imaging     = 1
    Diffraction = 2

# values for enumeration 'ProjectionSubMode'
class ProjectionSubMode(Enum):
    LM  = 1
    Mi  = 2
    SA  = 3
    Mh  = 4
    LAD = 5
    D   = 6

# values for enumeration 'HightensionState'
class HightensionState(Enum):
    Disabled = 1
    Off      = 2
    On       = 3

# values for enumeration 'GaugeStatus'
class GaugeStatus(Enum):
    Undefined = 0
    Underflow = 1
    Overflow  = 2
    Invalid   = 3
    Valid     = 4


# values for enumeration 'AcqShutterMode'
class AcqShutterMode(Enum):
    PreSpecimen  = 0
    PostSpecimen = 1
    Both         = 3


# values fo enumeration 'PlateLabelDateFormat'
class PlateLabelDateFormat(Enum):
    dtNoDate = 0
    dtDDMMYY = 1
    dtMMDDYY = 2
    dtYYMMDD = 3

# values for enumeration 'IlluminationMode'
class IlluminationMode(Enum):
    Nano  = 0
    Micro = 1

# values for enumeration 'CassetteSlotStatus'
class CassetteSlotStatus(Enum):
    Unknown  = 0
    Occupied = 1
    Empty    = 2
    Error    = 3

# values for enumeration 'StageStatus'
class StageStatus(Enum):
    Ready    = 0
    Disabled = 1
    NotReady = 2
    Going    = 3
    Moving   = 4
    Wobbling = 5

# values for enumeration 'instrumentMode'
class InstrumentMode(Enum):
    TEM  = 0
    STEM = 1


# values for enumeration 'AcqImageSize'
class AcqImageSize(Enum):
    Full    = 0
    Half    = 1
    Quarter = 2


# values for enumeration 'DarkFieldMode'
class DarkFieldMode(Enum):
    dfOff = 1
    dfCartesian = 2
    dfConical = 3


# values for enumeration 'GaugePressureLevel'
class GaugePressureLevel(Enum):
    plGaugePressureLevelUndefined = 0
    plGaugePressureLevelLow = 1
    plGaugePressureLevelLowMedium = 2
    plGaugePressureLevelMediumHigh = 3
    plGaugePressureLevelHigh = 4


# values for enumeration 'StageHolderType'
class StageHolderType(Enum):
    Empty      = 0
    SingleTilt = 1
    DoubleTilt = 2
    Invalid    = 4
    Polara     = 5
    DualAxis   = 6


# values for enumeration 'ScreenPosition'
class ScreenPosition(Enum):
    spUnknown =  1
    spUp = 2
    spDown = 3


# values for enumeration 'LensProg'
class LensProg(Enum):
    lpRegular = 1
    lpEFTEM = 2


# values for enumeration 'CondenserMode'
class CondenserMode(Enum):
    cmParallelIllumination = 0
    cmProbeIllumination = 1


# values for enumeration 'TEMScriptingError'
class TEMScriptingError(Enum):
    E_NOT_OK = -2147155969
    E_VALUE_CLIP = -2147155970
    E_OUT_OF_RANGE = -2147155971
    E_NOT_IMPLEMENTED = -2147155972


# values for enumeration 'MeasurementUnitType'
class MeasurementUnitType(Enum):
    MeasurementUnitType_Unknown = 0
    MeasurementUnitType_Meters = 1
    MeasurementUnitType_Radians = 2


# values for enumeration 'ProjectionDetectorShift'
class ProjectionDetectorShift(Enum):
     pdsOnAxis = 0
     pdsNearAxis = 1
     pdsOffAxis = 2


# values for enumeration 'ProjDetectorShiftMode'
class ProjDetectorShiftMode(Enum):
    pdsmAutoIgnore = 1
    pdsmManual = 2
    pdsmAlignment = 3


# values for enumeration 'AcqExposureMode'
class AcqExposureMode(Enum):
    AcqExposureMode_None = 0
    AcqExposureMode_Simultaneous = 1
    AcqExposureMode_PreExposure = 2
    AcqExposureMode_PreExposurePause = 3



# values for enumeration ''
class VacuumStatus(Enum):
    Unknown   = 1
    Off       = 2
    CameraAir = 3
    Busy      = 4
    Ready     = 6
    Else      = 6


# values for enumeration 'AcqImageCorrection'
class AcqImageCorrection(Enum):
    Unprocessed = 0
    Default = 1


# values for enumeration 'IlluminationNormalization'
class IlluminationNormalization(Enum):
    nmSpotsize = 1
    nmIntensity = 2
    nmCondenser = 3
    nmMiniCondenser = 4
    nmObjectivePole = 5
    nmAll = 6


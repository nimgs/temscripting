############### TITAN PROPERTIES ##############
import enums

class Titan(object):

    def __init__(self, com):
        self.com = com

    def get_titan_condensor_mode():
        return enums.CondensorMode(self.com.Illumination.CondensorMode)

    def set_titan_condensor_mode(mode=None):
        if mode in (enums.CondensorMode.imParallelIllumination.value,
                            enums.CondensorMode.imProbeIllumination.value
                            ):
            self.com.Illumination.CondensorMode = mode

    def get_titan_illuminated_area():
        return float(self.com.Illumination.IlluminatedArea.value)

    def set_titan_illuminated_area(meters=None):
        meters = float(meters)
        meters= map(sum, zip(meters, self.get_titan_illuminated_area()))
        self.com.Illumination.IlluminatedArea = meters

    def get_titan_probe_defocus():
        return float(self.com.Illumination.ProbeDefocus.value)

    def set_titan_probe_defocus(meters=None):
        meters = float(meters)
        meters = map(sum, zip(meters, self.get_titan_probe_defocus()))
        self.com.Illumination.ProbeDefocus = meters

    def get_titan_convergence_angle():
        return float(self.com.Illumination.ConvergenceAngle.value)

    def set_titan_convergence_angle(radians=None):
        radians = float(radians)
        radians = map(sum, zip(radians, self.get_titan_convergence_angle()))
        self.com.Illumination.ConvergenceAngle = radians

    def set_illumination_normalize(value=None):
        try:
            if enums.IlluminationNormalization(value):
                self.com.Illumination.Normalize(nm=value)
        except comtypes.COMError:
            raise ValueError

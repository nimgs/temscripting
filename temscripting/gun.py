########## GUN PROPERTIES ###########
import enums
import illumination

class Gun(object):

    def __init__(self, com):
        self.com = com

    def get_gun_tilt():
        ''' Returns a vector '''
        gt = self.com.Gun.Tilt.value
        return [gt.X, gt.Y]

    def set_gun_tilt(x=None, y=None):
        if Illumination.get_beam_blanker():
            try:
                gun_tilt = self.com.Gun.Tilt
                if x is not None and y is not None:
                    gun_tilt = map(sum, zip([float(x), float(y)], self.get_gun_tilt()))
                self.com.Gun.Tilt.X = gun_tilt[0]
                self.com.Gun.Tilt.Y = gun_tilt[1]
            except comtypes.COMError:
                raise ValueError

    def get_gun_shift():
        gs = self.com.Gun.Shift.value
        return [gs.X, gs.Y]

    def set_gun_shift(x=None, y=None):
        try:
            gun_shift = self.com.Gun.Shift
            if x is not None and y is not None:
                gun_shift = map(sum, zip([float(x), float(y)], self.get_gun_shift()))
                print "gs = {}, {}".format(gun_shift[0], gun_shift[1])
            self.com.Gun.Shift.X = gun_shift[0]
            self.com.Gun.Shift.Y = gun_shift[1]
        except comtypes.COMError:
            raise ValueError

    def get_high_tension_state():
        return self.com.Gun.HTState

    def set_high_tension_state(ht_state=None):
        self.com.Gun.HTState = ht_state

    def get_high_tension_value():
        ''' Returns volts '''
        return float(self.com.Gun.HTValue)

    def set_high_tension_value(volts=None):
        self.com.Gun.HTValue = float(volts)

    def get_high_tension_value():
        ''' Returns volts '''
        return float(self.com.Gun.HTMaxValue)

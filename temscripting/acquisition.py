########## ACQUISITION PROPERTIES ###########
import enums

class Acquisition(object):

    def __init__(self, com):
        self.com = com
        self.acq_object = None
        self.ccds = []
        self.detectors = []
        self.imgs = []

    def get_acquisition_object():
        try:
            self.acq_object = self.com.Acquisition
        except comtypes.COMError:
            raise ValueError

    def get_ccd_cameras():
        try:
           self.ccds = self.acq_object.Cameras
        except comtypes.COMError:
            raise ValueError

    def get_detectors():
        try:
            self.detectors = self.acq_object.Detectors
        except comtypes.COMError:
            raise ValueError

    def get_images():
        try:
            self.imgs =  self.acq_object.AcquireImages
        except comtypes.COMError:
            raise ValueError

    def add_acqusition_device(pDevice):
        try:
            self.acq_object.AddAcqDevice = pDevice
        except comtypes.COMError:
            raise ValueError

    def add_acquisition_device_by_name(pDevice):
        try:
            self.acq_object.AddAcqDeviceByName = str(pDevice)
        except comtypes.COMError:
            raise ValueError

    def remove_acquisition_device(pDevice):
        try:
            self.acq_object.RemoveAcqDevice = pDevice
        except comtypes.COMError:
            raise ValueError

    def remove_acquisiton_device(pDevice):
        try:
            self.acq_object.RemoveAcqDeviceByName = str(pDevice)
        except comtypes.COMError:
            raise ValueError

    def remove_all_acquistion_devices():
        try:
            self.acq_object.RemoveAllAcqDevices
        except comtypes.COMError:
            raise ValueError

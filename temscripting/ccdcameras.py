########## CCDCAMERAS PROPERTIES ###########
import enums
import acquisition


############### HELPER FUNCTIONS ##########
def get_nd_array(safearr=None):
    with safearray_as_ndarray:
        return safearr[0]

class CCDCameras(object):

    def __init__(self, com):
        self.com = com
        self.ccd_info = None
        self.acq_params = None
        self.ccd_acq_params = None
        self.detector_info = None
        self.detectors_acq_params = None

    def ccd_info():
        # required: user-selected camera
        self.ccd_info = acquisition.ccds.Info

    def ccd_acquisition_parameters():
        # required: user-selected camera
        self.ccd_acq_params = acquisition.ccds.AcqParams

    def get_ccd_info_name():
        try:
            return self.ccd_info.Name
        except comtypes.COMError:
            raise ValueError

    def get_ccd_info_height():
        try:
            return float(self.ccd_info.Height)
        except comtypes.COMError:
            raise ValueError

    def get_ccd_info_width():
        try:
            return float(self.ccd_info.Width)
        except comtypes.COMError:
            raise ValueError

    def get_ccd_info_pixel_size():
        try:
            return self.ccd_info.PixelSize
        except comtypes.COMError:
            raise ValueError

    def get_ccd_info_binnings():
        '''Returns a numpy array converted from a SafeArray
        with binning values supported by the CCD'''
        try:
            return get_nd_array(self.ccd_info.Binnings)
        except comtypes.COMError:
            raise ValueError

    def get_ccd_info_shutter_modes():
        '''Returns a numpy array converted from a SafeArray
        with the shutter modes supported by the CCD'''
        try:
            return get_nd_array(self.ccd_info.ShutterModes)
        except comtypes.COMError:
            raise ValueError

    def get_ccd_info_shutter_mode():
        ''' Get currently selected shutter mode of CCC '''
        try:
            return enums.AcqShutterMode(float(self.ccd_acq_params.ImageSize))
        except comtypes.COMError:
            raise ValueError

    def set_ccd_info_shutter_mode(shutter_mode=None):
        '''Return the currently selected shutter mode of the CCD '''
        if enums.AcqShutterMode(shutter_mode):
            self.ccd_info.ShutterMode = shutter_mode

    def get_ccd_info_binnings_as_variant():
        '''Return a ndarray converted from a VARIANT array with
        binning values supported by the CCD'''
        try:
            return get_nd_array(self.ccd_info.BinningsAsVariant)
        except comtypes.COMError:
            raise ValueError

    def get_ccd_info_shutter_modes_as_variant():
        '''Return a ndarray converted from a VARIANT array with
        shutter modes supported by the CCD '''
        try:
            return get_nd_array(self.ccd_info.ShutterModesAsVariant)
        except comtypes.COMError:
            raise ValueError

    def get_ccd_image_size():
        return enums.AcqImageSize(float(self.ccd_acq_params.ImageSize))

    def set_ccd_image_size(size=None):
        if enums.AcqImageSize(size):
            self.ccd_acq_params.ImageSize = size

    def get_ccd_exposure_time():
        return float(self.ccd_acq_params.ExposureTime)

    def set_ccd_exposure_time(sec=None):
        self.ccd_acq_params.ExposureTime =  float(sec)

    def get_ccd_binning():
        return float(self.ccd_acq_params.Binning)

    def set_ccd_binning(binning=None):
        self.ccd_acq_params.Binning = float(binning)

    def get_ccd_image_correction():
        return enums.AcqImageCorrection(self.ccd_acq_params.ImageCorrection)

    def set_ccd_image_correction(img_correction=None):
        if enums.AcqImageCorrection(img_correction):
            self.ccd_acq_params.ImageCorrection = img_correction

    def get_ccd_exposure_mode():
        return enums.AcqExposureMode(self.ccd_acq_params.ExposureMode)

    def set_ccd_exposure_mode(mode=None):
        if enums.AcqExposureMode(mode):
            self.ccd_acq_params.ExposureMode = mode

    def get_ccd_min_preexposure_time():
        return float(self.ccd_acq_params.MinPreExposureTime)

    def get_ccd_max_preexposure_time():
        return float(self.ccd_acq_params.MaxPreExposureTime)

    def get_ccd_min_preexposure_pause_time():
        return float(self.ccd_acq_params.MinPreExposurePauseTime)

    def get_ccd_max_preexposure_pause_time():
        return float(self.ccd_acq_params.MaxPreExposurePauseTime)

    def get_ccd_preexposure_time():
        return float(self.ccd_acq_params.PreExposureTime)

    def set_ccd_preexposure_time(sec=None):
        self.ccd_acq_params.PreExposureTime = float(sec)

    def get_ccd_preexposure_pause_time():
        return float(self.ccd_acq_params.PreExposurePauseTime)

    def set_ccd_preexposure_pause_time(sec=None):
        self.ccd_acq_params.PreExposurePauseTime = float(sec)

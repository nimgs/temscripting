########## INSTRUMENTMODECONTROL PROPERTIES ###########
import enums

class InstrumentModeControl(object):

    def __init__(self, com):
        self.com = com

    def get_instrument_stem_available():
        return self.com.InstrumentModeControl.value

    def get_instrument_mode():
        return enums.InstrumentMode(self.com.InstrumentModeControl.InstrumentMode)

    def set_instrument_mode(mode=None):
        if enums.InstrumentMode(mode):
            self.com.InstrumentModeControl.InstrumentMode = mode

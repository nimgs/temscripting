# drivers/tecnai.py
# python stdlib

# 3rd party
import zmq
import comtypes
import comtypes.client as cc
from comtypes.safearray import safearray_as_ndarray
import logging
import ctypes
import enums
#from stage import *

# nis code

tem= cc.CreateObject("TEMScripting.Instrument")
pressure_property = None



"""
API is on the same machine as the TEM server.

def get_api_version():
    pass

"""
############# Instrument Components as Properties #########
# tem.AutoLoader
# tem.Acquisition
# tem.AutoNormalizeEnabled
# tem.BlankerShutter
# tem.Gun
# tem.Illumination
# tem.InstrumentModeControl

# tem.Projection
# tem.Stage
# tem.StagePosition
# tem.TemperatureControl
# tem.UserButtons
# tem.Vacuum

############# Instrument Utility objects ###############
# pieces of info set together simultaneously
# tem.Vector
# tem.StagePosition

############### HELPER FUNCTIONS ##########
def get_nd_array(safearr=None):
    with safearray_as_ndarray:
        return safearr[0]

########## AUTOLOADER PROPERTIES ###########
def get_autoloader_status():
    return tem.AutoLoader.AutoLoaderAvailable

def get_number_of_cassette_slots():
    return float(tem.AutoLoader.NumberOfCassetteSlots)

def get_cassette_slot_status():
    slot_status = tem.AutoLoader.SlotStatus
    if slot_status in ( enums.CassetteSlotStatus.CassetteSlotStatus_Unknown.value,
                                enums.CassetteSlotStatus.CassetteSlotStatus_Occupied.value,
                                enums.CassetteSlotStatus.CassetteSlotStatus_Empty.value,
                                enums.CassetteSlotStatus.CassetteSlotStatus_Error.value
                                ):
        return slot_status
    else:
        return ValueError

def load_cartridge(value):
    tem.AutoLoader.LoadCartridge = float(value)

def unload_cartridge():
    tem.AutoLoader.UnloadCartridge

def perform_cassette_inventory():
    return tem.AutoLoader.PerformCassetteInventory

########## AUTONORMALIZEENABLED NORMALIZEALL PROPERTY ###########

def get_autonormalize_enabled():
    return tem.AutoNormalizeEnabled

def set_autonormalize_enabled(value=None):
    try:
        tem.AutoNormalizeEnabled = value
    except comtypes.COMError:
        raise ValueError

def set_normalize_all():
    tem.NormalizeAll()

########## ACQUISITION PROPERTIES ###########
acq_object = None
ccds = []
detectors = []
imgs = []

def get_acquisition_object():
    try:
        acq_object = tem.Acquisition
    except Exception:
        raise ValueError

def get_ccd_cameras():
    try:
        ccds = acq_object.Cameras
    except Exception:
        raise ValueError

def get_detectors():
    try:
        detectors = acq_object.Detectors
    except Exception:
        raise ValueError

def get_images():
    try:
        imgs =  acq_object.AcquireImages
    except Exception:
        raise ValueError

def add_acqusition_device(pDevice):
    acq_object.AddAcqDevice = pDevice

def add_acquisition_device_by_name(pDevice):
    acq_object.AddAcqDeviceByName = str(pDevice)

def remove_acquisition_device(pDevice):
    acq_object.RemoveAcqDevice = pDevice

def remove_acquisiton_device(pDevice):
    acq_object.RemoveAcqDeviceByName = str(pDevice)

def remove_all_acquistion_devices():
    acq_object.RemoveAllAcqDevices


########## CCDCAMERAS PROPERTIES ###########
ccd_info = None
acq_params = None
ccd_acq_params = None
detector_info = None
detectors_acq_params = None

def ccd_info():
    # required: user-selected camera
    ccd_info = ccds.Info

def ccd_acquisition_parameters():
    # required: user-selected camera
    ccd_acq_params = ccds.AcqParams

def get_ccd_info_name():
    try:
        return ccd_info.Name
    except Exception:
        raise ValueError

def get_ccd_info_height():
    try:
        return float(ccd_info.Height)
    except Exception:
        raise ValueError

def get_ccd_info_width():
    try:
        return float(ccd_info.Width)
    except Exception:
        raise ValueError

def get_ccd_info_pixel_size():
    try:
        return ccd_info.PixelSize
    except Exception:
        raise ValueError

def get_ccd_info_binnings():
    '''Returns a numpy array converted from a SafeArray
    with binning values supported by the CCD'''
    try:
        return get_nd_array(ccd_info.Binnings)
    except Exception:
        raise ValueError

def get_ccd_info_shutter_modes():
    '''Returns a numpy array converted from a SafeArray
    with the shutter modes supported by the CCD'''
    try:
        return get_nd_array(ccd_info.ShutterModes)
    except Exception:
        raise ValueError

def get_ccd_info_shutter_mode():
    ''' Get currently selected shutter mode of CCC '''
    shutter_mode= float(ccd_acq_params.ImageSize)
    if shutter_mode in ( enums.AcqShutterMode.AcqShutterMode_PreSpecimen.value,
                         enums.AcqShutterMode.AcqShutterMode_PostSpecimen.value,
                         enums.AcqShutterMode.AcqShutterMode_Both.value ):
        return shutter_mode
    else:
        return ValueError

def set_ccd_info_shutter_mode(shutter_mode=None):
    '''Return the currently selected shutter mode of the CCD '''
    if shutter_mode in ( enums.AcqShutterMode.AcqShutterMode_PreSpecimen.value,
                                    enums.AcqShutterMode.AcqShutterMode_PostSpecimen.value,
                                    enums.AcqShutterMode.AcqShutterMode_Both.value
                                    ):
        ccd_info.ShutterMode = shutter_mode

def get_ccd_info_binnings_as_variant():
    '''Return a ndarray converted from a VARIANT array with
    binning values supported by the CCD'''
    try:
        return get_nd_array(ccd_info.BinningsAsVariant)
    except Exception:
        raise ValueError

def get_ccd_info_shutter_modes_as_variant():
    '''Return a ndarray converted from a VARIANT array with
    shutter modes supported by the CCD '''
    try:
        return get_nd_array(ccd_info.ShutterModesAsVariant)
    except Exception:
        raise ValueError

def get_ccd_image_size():
    image_size= float(ccd_acq_params.ImageSize)
    if image_size in ( enums.AcqImageSize.AcqImageSize_Full.value,
                                enums.AcqImageSize.AcqImageSize_Half.value,
                                enums.AcqImageSize.AcqImageSize_Quarter.value
                                ):
        return image_size
    else:
        return ValueError

def set_ccd_image_size(size=None):
    if size in ( enums.AcqImageSize.AcqImageSize_Full.value,
                    enums.AcqImageSize.AcqImageSize_Half.value,
                    enums.AcqImageSize.AcqImageSize_Quarter.value
                    ):
        ccd_acq_params.ImageSize = size

def get_ccd_exposure_time():
    return float(ccd_acq_params.ExposureTime)

def set_ccd_exposure_time(sec=None):
    ccd_acq_params.ExposureTime =  float(sec)

def get_ccd_binning():
    return float(ccd_acq_params.Binning)

def set_ccd_binning(binning=None):
    ccd_acq_params.Binning = float(binning)

def get_ccd_image_correction():
    img_correction = ccd_acq_params.ImageCorrection
    if img_correction in ( enums.AcqImageCorrection.AcqImageCorrection_Unprocessed.value,
                                        enums.AcqImageCorrection.AcqImageCorrection_Default.value
                                        ):
        return img_correction
    else:
        raise ValueError

def set_ccd_image_correction(img_correction=None):
    if img_correction in ( enums.AcqImageCorrection.AcqImageCorrection_Unprocessed.value,
                                        enums.AcqImageCorrection.AcqImageCorrection_Default.value
                                        ):
        ccd_acq_params.ImageCorrection = img_correction

def get_ccd_exposure_mode():
    exposure_mode = ccd_acq_params.ExposureMode
    if exposure_mode in ( enums.AcqExposureMode.AcqExposureMode_None.value,
                                        enums.AcqExposureMode.AcqExposureMode_Simultaneous.value,
                                        enums.AcqExposureMode.AcqExposureMode_PreExposure.value,
                                        enums.AcqExposureMode.AcqExposureMode_PreExposurePause.value
                                        ):
        return exposure_mode
    else:
        raise ValueError

def set_ccd_exposure_mode(mode=None):
    if mode in ( enums.AcqExposureMode.AcqExposureMode_None.value,
                        enums.AcqExposureMode.AcqExposureMode_Simultaneous.value,
                        enums.AcqExposureMode.AcqExposureMode_PreExposure.value,
                        enums.AcqExposureMode.AcqExposureMode_PreExposurePause.value
                        ):
        ccd_acq_params.ExposureMode = mode

def get_ccd_min_preexposure_time():
    return float(ccd_acq_params.MinPreExposureTime)

def get_ccd_max_preexposure_time():
    return float(ccd_acq_params.MaxPreExposureTime)

def get_ccd_min_preexposure_pause_time():
    return float(ccd_acq_params.MinPreExposurePauseTime)

def get_ccd_max_preexposure_pause_time():
    return float(ccd_acq_params.MaxPreExposurePauseTime)

def get_ccd_preexposure_time():
    return float(ccd_acq_params.PreExposureTime)

def set_ccd_preexposure_time(sec=None):
    ccd_acq_params.PreExposureTime = float(sec)

def get_ccd_preexposure_pause_time():
    return float(ccd_acq_params.PreExposurePauseTime)

def set_ccd_preexposure_pause_time(sec=None):
    ccd_acq_params.PreExposurePauseTime = float(sec)


############## DETECTORS ##############
def detector_info():
    ''' Required: user-selected detector '''
    detector_info = detectors.Info

def detectors_acquisition_parameters():
    detector_acq_params = detectors.AcqParams

def get_detector_info_name():
    return detector_info.Name

def get_detector_info_brightness():
    return float(detector_info.Brightness)

def set_detector_info_brightness(brightness=None):
    detector_info.Brightness = float(brightness)

def get_detector_info_contrast():
    return float(detector_info.Contrast)

def set_detector_info_contrast(contrast=None):
    detector_info.Contrast = float(contrast)

def get_detector_info_binnings():
    ''' Returns a numpy array converted from a SafeArray
    with the binning values supported by the STEM detector '''
    return get_nd_array(detector_info.Binnings)

def get_detector_info_binnings_as_variant():
    '''Return a numpy array converted from a VARIANT array
    with binning values supported by the STEM detector '''
    return get_nd_array(detector_info.BinningsAsVariant)

def get_detector_image_size():
    image_size= float(detector_acq_params.ImageSize)
    if image_size in ( enums.AcqImageSize.AcqImageSize_Full.value,
                                enums.AcqImageSize.AcqImageSize_Half.value,
                                enums.AcqImageSize.AcqImageSize_Quarter.value
                                ):
        return image_size
    else:
        raise ValueError

def set_detector_image_size(size=None):
    if size in ( enums.AcqImageSize.AcqImageSize_Full.value,
                     enums.AcqImageSize.AcqImageSize_Half.value,
                     enums.AcqImageSize.AcqImageSize_Quarter.value
                    ):
        detector_acq_params.ImageSize = size

def get_detector_dwell_time():
    return float(detectors_acq_params.DwellTime)

def set_detector_dwell_time(sec=None):
    detectors_acq_params.DwellTime =  float(sec)

########## BLANKERSHUTTER PROPERTIES ###########
def get_blankershutter_overrideon():
    print 'bs = {}'.format(tem.BlankerShutter.ShutterOverrideOn)
    return tem.BlankerShutter.ShutterOverrideOn

def set_blankershutter_overrideon(value=None):
    try:
        tem.BlankerShutter.ShutterOverrideOn = value
    except comtypes.COMError:
        raise ValueError

########## (PLATE) CAMERA PROPERTIES ###########

def get_camera_main_screen():
    return tem.Camera.MainScreen

def set_camera_main_screen(value=None):
    position = tem.Camera.MainScreen
    try:
        if position in ( enums.ScreenPosition.spUnknown.value,
                                enums.ScreenPosition.spUp.value,
                                enums.ScreenPosition.spDown.value
                                ):
            tem.Camera.MainScreen = value
    except comtypes.COMError:
        raise ValueError

def get_is_small_screen_down():
    return tem.Camera.IsSmallScreenDown

def get_stock():
    return float(tem.Camera.Stock)

def get_measured_exposure_time():
    return float(tem.Camera.MeasuredExposureTime)

def get_manual_exposure_time():
    return float(tem.Camera.ManualExposureTime)

def set_manual_exposure_time(sec=None):
    tem.Camera.ManualExposureTime = float(sec)

def get_manual_exposure():
    return tem.Camera.ManualExposure

def set_manual_exposure(value=None):
    tem.Camera.ManualExposure = value

def get_film_text():
    return tem.Camera.FilmText

def set_film_text(text=None):
    tem.Camera.FilmText = text

def get_exposure_number():
    return float(tem.Camera.ExposureNumber)

def set_exposure_number(value=None):
    tem.Camera.ExposureNumber = float(value)

def get_user_code():
    return tem.Camera.UserCode

def set_user_code(code=None):
    tem.Camera.UserCode = code

def get_plate_label_date_type():
    label = tem.Camera.PlateLabelDateType
    if label in ( enums.PlateLabelDateFormat.dtNoDate.value,
                        enums.PlateLabelDateFormat.dtDDMMYY.value,
                        enums.PlateLabelDateFormat.dtMMDDYY.value,
                        enums.PlateLabelDateFormat.dtYYMMDD.value
                    ):
        return label
    else:
        return ValueError

def set_plate_label_date_type(value=None):
    if value in ( enums.PlateLabelDateFormat.dtNoDate.value,
                        enums.PlateLabelDateFormat.dtDDMMYY.value,
                        enums.PlateLabelDateFormat.dtMMDDYY.value,
                        enums.PlateLabelDateFormat.dtYYMMDD.value
                    ):
        tem.Camera.PlateLabelDateType = value

def get_screen_dim_text():
    return tem.Camera.ScreenDimText

def set_screen_dim_text(value=None):
    tem.Camera.ScreenDimText = value

def get_screen_dim():
    return tem.Camera.ScreenDim

def set_screen_dim(value=None):
    tem.Camera.ScreenDim = value

def get_screen_current():
    return float(tem.Camera.ScreenCurrent)

def set_screen_current(amperes=None):
    tem.Camera.ScreenCurrent = float(amperes)

def take_exposure():
    tem.Camera.TakeExposure

########## CONFIGURATION PROPERTIES ###########
# As of Version Tecnai 4.5 or higher and Titan 1.5 or higher
#def get_product_family():
#    product = tem.Configuration.ProductFamily()
#    if product == win32com.client.constants.ProductFamily_Tecnai:
#        return 'tecnai'
#    elif product == win32com.client.constants.ProductFamily_Titan:
#        return 'titan'


########## GUN PROPERTIES ###########
def get_gun_tilt():
    ''' Returns a vector '''
    gt = tem.Gun.Tilt.value
    return [gt.X, gt.Y]

def set_gun_tilt(x=None, y=None):
    if get_beam_blanker():
        try:
            gun_tilt = tem.Gun.Tilt
            if x is not None and y is not None:
                gun_tilt = map(sum, zip([float(x), float(y)], get_gun_tilt()))
            tem.Gun.Tilt.X = gun_tilt[0]
            tem.Gun.Tilt.Y = gun_tilt[1]
        except comtypes.COMError:
            raise ValueError

def get_gun_shift():
    gs = tem.Gun.Shift.value
    return [gs.X, gs.Y]

def set_gun_shift(x=None, y=None):
    try:
        gun_shift = tem.Gun.Shift
        if x is not None and y is not None:
            gun_shift = map(sum, zip([float(x), float(y)], get_gun_shift()))
            print "gs = {}, {}".format(gun_shift[0], gun_shift[1])
        tem.Gun.Shift.X = gun_shift[0]
        tem.Gun.Shift.Y = gun_shift[1]
    except comtypes.COMError:
        raise ValueError

def get_high_tension_state():
    return tem.Gun.HTState

def set_high_tension_state(ht_state=None):
    tem.Gun.HTState = ht_state

def get_high_tension_value():
    ''' Returns volts '''
    return float(tem.Gun.HTValue)

def set_high_tension_value(volts=None):
    tem.Gun.HTValue = float(volts)

def get_high_tension_value():
    ''' Returns volts '''
    return float(tem.Gun.HTMaxValue)


########## ILLUMINATION PROPERTIES ###########
def get_mode():
    mode = tem.Illumination.Mode
    if mode in (enums.IlluminationMode.imMicroprobe.value,
                        enums.IlluminationMode.imNanoprobe.value
                        ):
        return mode
    else:
        return ValueError

def set_mode(value=None):
    if value in ( enums.IlluminationMode.imMicroprobe.value,
                        enums.IlluminationMode.imNanoprobe.value
                        ):
        tem.Illumination.Mode = value

def get_df_mode():
    mode = tem.Illumination.DFMode
    if mode in (enums.DarkFieldMode.dfOff.value,
                        enums.DarkFieldMode.dfCartesian.value,
                        enums.DarkFieldMode.dfConical.value
                        ):
        return mode
    else:
        return ValueError

def set_df_mode(value=None):
    if value in (enums.DarkFieldMode.dfOff.value,
                        enums.DarkFieldMode.dfCartesian.value,
                        enums.DarkFieldMode.dfConical.value
                    ):
        tem.Illumination.DFMode = value

def get_beam_blanker():
    return tem.Illumination.BeamBlanked

def set_beam_blanker(value=None):
    try:
        tem.Illumination.BeamBlanked = value
    except comtypes.COMError:
        raise ValueError

def get_condenser_stigmator():
    vec = tem.Illumination.CondensorStigmator
    return [vec.X, vec.Y]

def set_condenser_stigmator(x=None, y=None):
    # range: -1.0 to +1.0
    stigmator = [float(x), float(y)]
    stigmator = map(sum, zip(stigmator, get_condenser_stigmator()))
    tem.Illumination.CondenserStigmator = stigmator

def get_spot_size_index():
    return tem.Illumination.SpotsizeIndex

def set_spot_size_index(index=0):
    tem.Illumination.SpotsizeIndex = index

def get_intensity():
    return tem.Illumination.Intensity

def set_intensity(value=None):
    # some microscopes minimum is higher than
    tem.Illumination.Intensity = value

def get_intensity_zoom_enabled():
    return tem.Illumination.IntensityZoomEnabled

def set_intensity_zoom_enabled(enable=None):
    tem.Illumination.IntensityZoomEnabled = enabled

def get_intensity_limit_enabled():
    return tem.Illumination.IntensityLimitEnabled

def set_intensity_limit_enabled(enable=None):
    tem.Illumination.IntensityLimitEnabled = enable

def get_illumination_shift():
    vec = tem.Illumination.Shift.value
    return [vec.X, vec.Y]

def set_illumination_shift(x=None, y=None):
    shift = [float(x), float(y)]
    shift = map(sum, zip(shift, get_illumination_shift()))
    tem.Illumination.Shift = shift

def get_illumination_tilt():
    vec = tem.Illumination.Tilt.value
    return [vec.X, vec.Y]

def set_illumination_tilt(x=None, y=None):
    tilt = [float(x), float(y)]
    tilt = map(sum, zip(tilt, get_illumination_tilt()))
    tem.Illumination.Tilt = tilt

def get_illumination_rotation_center():
    vec = tem.Illumination.RotationCenter
    return [vec.X, vec.Y]

def set_illumination_rotation_center(x=None, y=None):
    rotation = [float(x), float(y)]
    rotation = map(sum, zip(rotation, get_illumination_rotation_center()))
    tem.Illumination.RotationCenter = rotation

def get_illumination_stem_magnification():
    return float(tem.Illumination.StemMagnification)

def set_illumination_stem_magnification(value=None):
    magnification = tem.Illumination.StemMagnification
    magnification = float(value)
    try:
        tem.Illumination.StemMagnification = magnification
    except comtypes.COMError:
        raise ValueError

def get_illumination_stem_rotation():
    return float(tem.Illumination.StemRotation)

def set_illumination_stem_rotation(rotation=None):
    rotation = float(rotation)
    rotation = map(sum, zip(rotation, get_illumination_stem_rotation()))
    tem.Illumination.StemRotation = rotation

############### TITAN PROPERTIES ##############
def get_titan_condensor_mode():
    mode =  tem.Illumination.CondensorMode
    if mode in (enums.CondensorMode.imParallelIllumination.value,
                        enums.CondensorMode.imProbeIllumination.value
                        ):
        return mode
    else:
        return ValueError

def set_titan_condensor_mode(mode=None):
    if mode in (enums.CondensorMode.imParallelIllumination.value,
                        enums.CondensorMode.imProbeIllumination.value
                        ):
        tem.Illumination.CondensorMode = mode

def get_titan_illuminated_area():
    return float(tem.Illumination.IlluminatedArea.value)

def set_titan_illuminated_area(meters=None):
    meters = float(meters)
    meters= map(sum, zip(meters, get_titan_illuminated_area()))
    tem.Illumination.IlluminatedArea = meters

def get_titan_probe_defocus():
    return float(tem.Illumination.ProbeDefocus.value)

def set_titan_probe_defocus(meters=None):
    meters = float(meters)
    meters = map(sum, zip(meters, get_titan_probe_defocus()))
    tem.Illumination.ProbeDefocus = meters

def get_titan_convergence_angle():
    return float(tem.Illumination.ConvergenceAngle.value)

def set_titan_convergence_angle(radians=None):
    radians = float(radians)
    radians = map(sum, zip(radians, get_titan_convergence_angle()))
    tem.Illumination.ConvergenceAngle = radians

def set_illumination_normalize(value=None):
    try:
        if value in ( enums.IlluminationNormalization.nmSpotsize.value,
                            enums.IlluminationNormalization.nmIntensity.value,
                            enums.IlluminationNormalization.nmCondenser.value,
                            enums.IlluminationNormalization.nmMiniCondenser.value,
                            enums.IlluminationNormalization.nmObjectivePole.value,
                            enums.IlluminationNormalization.nmAll.value,
                        ):
            tem.Illumination.Normalize(nm=value)
    except comtypes.COMError:
        return ValueError

########## INSTRUMENTMODECONTROL PROPERTIES ###########
def get_instrument_stem_available():
    return tem.InstrumentModeControl.value

def get_instrument_mode():
    mode = tem.InstrumentModeControl.InstrumentMode
    if mode in (enums.InstrumentMode.InstrumentMode_STEM.value,
                        enums.InstrumentMode.InstrumentMode_TEM.value
                    ):
        return mode
    else:
        return ValueError

def set_instrument_mode(mode=None):
    if mode in (enums.InstrumentMode.InstrumentMode_STEM.value,
                        enums.InstrumentMode.InstrumentMode_TEM.value
                        ):
        tem.InstrumentModeControl.InstrumentMode = mode

########## PROJECTION PROPERTIES ###########
def get_projection_mode():
    mode = tem.Projection.Mode
    if mode in (enums.ProjectionMode.pmImaging.value,
                    enums.ProjectionMode.pmDiffraction.value
                    ):
        return mode
    else:
        return ValueError

def set_projection_mode(mode=None):
    if mode in (enums.ProjectionMode.pmImaging.value,
                    enums.ProjectionMode.pmDiffracation.value
                    ):
        tem.Projection.Mode = mode

def get_projection_submode():
    mode == tem.Projection.SubMode
    if mode in (enums.ProjectionSubMode.psmLM.value,
                    enums.ProjectionSubMode.psmMi.value,
                    enums.ProjectionSubMode.psmSA.value,
                    enums.ProjectionSubMode.psmMh.value,
                    enums.ProjectionSubMode.psmLAD.value,
                    enums.ProjectionSubMmode.psmD.value
                    ):
        return mode
    else:
        return ValueError

def get_projection_submode_string():
    return tem.Projection.SubModeString

def get_projection_lens_program():
    program = tem.Projection.LensProgram
    if program in (enums.LensProg.lpRegular.value,
                            enums.LensProg.lpEFTEM.value
                            ):
        return program
    else:
        return ValueError

def set_projection_lens_program(program=None):
    if program in (enums.LensProg.lpRegular.value,
                            enums.LensProg.lpEFTEM.value
                            ):
        tem.Projection.LensProgram = program

def get_projection_magnification():
    return float(tem.Projection.Magnification)

def get_projection_magnification_index():
    return tem.Projection.MagnificationIndex

def set_projection_magnification_index(value=None):
    index = tem.Projection.MagnificationIndex
    index = value
    try:
        tem.Projection.MagnificationIndex = index
    except comtypes.COMError:
        raise ValueError

def get_projection_image_rotation():
    return float(tem.Projection.ImageRotation)

def get_projection_detector_shift():
    shift = tem.Projection.DetectorShift
    if shift in ( enums.ProjectionDetectorShift.pdsOnAxis.value,
                    enums.ProjectionDetectorShift.pdsNearAxis.value,
                    enums.ProjectionDetectorShift.pdsOffAxis.value
                 ):
        return shift
    else:
        return ValueError

def set_projection_detector_shift(shift=None):
    if shift in (enums.ProjectionDetectorShift.pdsOnAxis.value,
                    enums.ProjectionDetectorShift.pdsNearAxis.value,
                    enums.ProjectionDetectorShift.pdsOffAxis.value
                 ):
        tem.Projection.DetectorShift = shift

def get_projection_detector_shift_mode():
    mode = tem.Projection.DetectorShiftMode
    if mode in  (enums.ProjDetectorShiftMode.pdsmAutoIgnore.value,
                        enums.ProjDetectorShiftMode.pdsmManual.value,
                        enums.ProjDetectorShiftMode.pdsmAlignment.value
                    ):
        return mode
    else:
        return ValueError


def set_projection_detector_shift_mode(mode=None):
    if mode in (enums.ProjDetectorShiftMode.pdsmAutoIgnore.value,
                    enums.ProjDetectorShiftMode.pdsmManual.value,
                    enums.ProjDetectorShiftMode.pdsmAlignment.value
                    ):
        tem.Projection.DetectorShiftMode = mode

def get_projection_focus():
    return float(tem.Projection.Focus)

def set_projection_focus(value=None):
    focus = tem.Projection.Focus
    focus = float(value)
    try:
        tem.Projection.Focus = focus
    except comtypes.COMError:
        raise ValueError

def get_projection_defocus():
    return float(tem.Projection.Defocus)

def set_projection_defocus(value=None):
    defocus = tem.Projection.Defocus
    defocus = float(value)
    print 'dval: {}'.format(defocus)
    try:
        tem.Projection.Defocus = defocus
    except comtypes.COMError:
        raise ValueError

def get_projection_objective_excitation():
    return float(tem.Projection.ObjectiveExcitation)

def get_projection_camera_length():
    return float(tem.Projection.CameraLength)

def get_projection_camera_length_index():
    return tem.Projection.CameraLengthIndex

def set_projection_camera_length_index(index=None):
    tem.Projection.CameraLengthIndex = index

def get_projection_objective_stigmator():
    vec = tem.Projection.ObjectiveStigmator
    return [vec.X, vec.Y]

def set_projection_objective_stigmator(x=None, y=None):
    stigmator = [float(x), float(y)]
    stigmator = map(sum, zip(stigmator, get_projection_objective_stigmator()))
    tem.Projection.ObjectiveStigmator = stigmator

def get_projection_diffraction_stigmator():
    vec = tem.Projection.DiffractionStigmator
    return [vec.X, vec.Y]

def set_projection_diffraction_stigmator(x=None, y=None):
    stigmator = [float(x), float(y)]
    stigmator = map(sum, zip(stigmator, get_projection_diffraction_stigmator()))
    tem.Projection.DiffractionStigmator = stigmator

def get_projection_diffraction_shift():
    vec = tem.Projection.DiffractionShift
    return [vec.X, vec.Y]

def set_projection_diffraction_shift(x=None, y=None):
    shift = [float(x), float(y)]
    shift = map(sum, zip(shift, get_projection_diffraction_shift()))
    tem.Projection.DiffractionShift = shift

def get_projection_image_shift():
    vec = tem.Projection.ImageShift
    print 'vecx: {} vecy: {}'.format(vec.X, vec.Y)
    return [vec.X, vec.Y]

def set_projection_image_shift(x=None, y=None):
    shift = [float(x), float(y)]
    shift = map(sum, zip(shift, get_projection_image_shift()))
    print 'shift[0]: {}, shift[1]: {}'.format(shift[0], shift[1])
    pis = tem.Projection.ImageShift
    try:
        pis.X = shift[0]
        pis.Y = shift[1]
        tem.Projection.ImageShift = pis
    except comtypes.COMError:
        raise ValueError

def get_projection_image_beam_shift():
    vec = tem.Projection.ImageBeamShift
    print type(vec)
    print 'vecx: {} vecy: {}'.format(vec.X, vec.Y)
    return [vec.X, vec.Y]

def set_projection_image_beam_shift(x=None, y=None):
    shift = [float(x), float(y)]
    shift = map(sum, zip(shift, get_projection_image_beam_shift()))
    print 'shift[0]: {}, shift[1]: {}'.format(shift[0], shift[1])
    pibs = tem.Projection.ImageBeamShift
    try:
        pibs.X = shift[0]
        pibs.Y = shift[1]
        tem.Projection.ImageBeamShift = pibs
        #print 'X = {}'.format(tem.Projection.ImageBeamShift.X)
    except comtypes.COMError:
        raise ValueError

def get_projection_image_beam_tilt():
    vec = tem.Projection.ImageBeamTilt
    return [vec.X, vec.Y]

def set_projection_image_beam_tilt(x=None, y=None):
    tilt = [float(x), float(y)]
    tilt= map(sum, zip(shift, get_projection_image_beam_tilt()))
    tem.Projection.ImageBeamTilt = tilt

def get_projection_index():
    return tem.Projection.ProjectionIndex

def set_projection_index(index=None):
    tem.Projection.ProjectionIndex = index

def get_projection_submode_minindex():
    return tem.Projection.SubModeMinIndex

def get_projection_submode_maxindex():
    return tem.Projection.SubModeMaxIndex

def set_projection_reset_defocus():
    tem.Projection.ResetDefocus()
    #set_projection_defocus(value=value)

def set_change_projection_index(steps=None):
    tem.Projection.ChangeProjectionIndex = steps

def set_projection_normalize(value=None):
    try:
        if value in ( enums.ProjectionNormalization.pnmObjective.value,
                            enums.ProjectionNormalization.pnmProjector.value,
                            enums.ProjectionNormalization.pnmAll.value
                        ):
            tem.Projection.Normalize(norm=value)
    except comtypes.COMError:
        return ValueError



########## STAGE PROPERTIES ###########

def get_stage_status():
    status = tem.Stage.Status
    if status in ( enums.StageStatus.stReady.value,
                         enums.StageStatus.stDisabled.value,
                         enums.StageStatus.stNotReady.value,
                         enums.StageStatus.stGoing.value,
                         enums.StageStatus.stMoving.value,
                         enums.StageStatus.stWobbling.value
                        ):
        return status

def get_stage_position():
    '''Return utility StagePosition object to get/set the current
      position of the stage and return position as an array
      of 5 doubles
    '''
    curr_pos = tem.Stage.Position.value
    print curr_pos
    #return curr_pos # can't pickle StagePosition position
    return [curr_pos.X, curr_pos.Y, curr_pos.Z, curr_pos.A, curr_pos.B ]

def set_stage_position(X=None, Y=None, Z=None, A=None, B=None):
    go_to(X=X, Y=Y, Z=Z, A=A, B=B)

def get_stage_holder():
    holder = tem.Stage.Holder
    if holder in (enums.StageHolderType.hoInvalid.value,
                         enums.StageHolderType.hoSingleTilt.value,
                         enums.StageHolderType.hoDoubleTilt.value,
                         enums.StageHolderType.hoNone.value,
                         enums.StageHolderType.hoPolara.value,
                         enums.StageHolderType.hoDualAxis.value
                        ):
        return holder
    else:
        return ValueError


def get_stage_axis_data():
    return tem.Stage.AxisData

def go_to(X=None, Y=None, Z=None, A=None, B=None):

    sp = tem.Stage.Position
    if X is not None: sp.X = float(X)
    if Y is not None: sp.Y = float(Y)
    if Z is not None: sp.Z = float(Z)
    if A is not None: sp.A = float(A)
    if B is not None: sp.B = float(B)

    print 'sp: %s'%sp
    print '  ', sp.X
    print '  ', sp.Y
    print '  ', sp.Z
    print '  ', sp.A

    axes = 0
    if X is not None and Y is not None:
        axes |= 3
    if Y is not None:
        axes |= 2
    if X is not None:
        axes |= 1
    if Z is not None:
        axes |= 4
    if A is not None:
        axes |= 8
    if B is not None:
        axes |= 16

    print 'axes = %s'%axes

    try:
        tem.Stage.Goto(sp, axes)
    except comtypes.COMError:
        status = get_stage_status()
        if status == enums.StageStatus.stNotReady:
            raise StageNotReady('Error %s: Stage is not ready'%status)
        elif status == enums.StageStatus.stDisabled:
            raise StageNotReady('Error %s: Stage is disabled'%status)
        elif status == enums.StageStatus.stWobbling:
            raise StageNotReady('Error %s: Stage is wobbling'%status )

class StageNotReady(Exception):
    pass


def move_to(next_pos=None, stage_axes=None):
    new_pos = [0.0, 0.0, 0.0, 0.0, 0.0]
    curr_pos = get_stage_position()
    new_pos = map(sum, zip(next_pos, curr_pos))

    axes = 0
    if stage_axes is not None:
        for item in ('B', 'A', 'Z', 'XY', 'Y', 'X'):
            axes |= getattr(enums.StageAxes, 'axis' + item).value

    tem.Stage.MoveTo(new_pos[0], new_pos[1], new_pos[2], new_pos[3], new_pos[4], axes)




########## TEMPERATURECONTROL PROPERTIES ###########
def get_temperature_control_available():
    return tem.TemperatureControl.TemperatureControlAvailable

def get_refrigerant_level():
    fridge_level = float(tem.TemperatureControl.RefrigerantLevel)
    if fridge_level in (enums.RefrigerantLevel.RefrigerantLevel_AutoLoaderDewar.value,
                                 enums.RefrigerantLevel.RefrigerantLevel_ColumnDewar.value,
                                 enums.RefrigerantLevel.RefrigerantLevel_HeliumDewar.value
                                ):
        return fridge_level
    else:
        return ValueError

def temperature_control_force_refill():
    tem.TemperatureControl.ForceRefill

# not available on TEM Server
#def get_dewars_remaining_time():
#    return tem.TemperatureControl.DewarsRemainingTime

#def get_dewars_are_busy_filling():
#    return tem.TemperatureControl.DewarsAreBusyFilling


########## USERBUTTONS PROPERTIES ###########
def get_user_button_name():
    return tem.UserButton.Name

def get_user_button_label():
    return tem.UserButton.Label

def get_user_button_assignment():
    return tem.UserButton.Assignment

def set_user_button_assignment(label=None):
    tem.UserButton.Assignment = label

def user_button_pressed():
    tem.UserButton.Pressed

########## VACUUM PROPERTIES ###########
def get_vacuum_status():
    status = tem.Vacuum.Status
    if status in ( enums.VacuumStatus.vsOff.value,
                    enums.VacuumStatus.vsCameraAir.value,
                    enums.VacuumStatus.vsBusy.value,
                    enums.VacuumStatus.vsReady.value,
                    enums.VacuumStatus.vsUnknown.value,
                    enums.VacuumStatus.vsElse.value
                  ):
        return status
    else:
        return ValueError

def get_vacuum_column_valves_open():
    return tem.Vacuum.ColumnValvesOpen

def set_vacuum_column_valves_open(state=None):
    tem.Vacuum.ColumnValvesOpen = state

def get_vacuum_pvp_running():
    return tem.Vacuum.PVPRunning

def get_vacuum_gauges():
    return tem.Vacuum.Gauges

def vacuum_run_buffer_cycle():
    tem.Vacuum.RunBufferCycle
